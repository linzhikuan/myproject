package com.example.administrator.myproject;

import android.graphics.Color;
import android.os.Build;
import android.provider.CalendarContract;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.FrameLayout;

import cn.finalteam.loadingviewfinal.ListViewFinal;
import cn.finalteam.loadingviewfinal.OnDefaultRefreshListener;
import cn.finalteam.loadingviewfinal.OnLoadMoreListener;
import cn.finalteam.loadingviewfinal.PtrClassicFrameLayout;
import cn.finalteam.loadingviewfinal.PtrFrameLayout;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private SystemBarTintManager tintManager;
    private ListViewFinal mLvGames;
    private PtrClassicFrameLayout mPtrLayout;
    private FrameLayout mFlEmptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setStatusBar(true);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mLvGames = (ListViewFinal) findViewById(R.id.lv_games);
        mPtrLayout = (PtrClassicFrameLayout) findViewById(R.id.ptr_layout);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        mLvGames.setEmptyView(mFlEmptyView);
        mPtrLayout.setOnRefreshListener(new OnDefaultRefreshListener() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                mLvGames.setAdapter(new ListAdapter(MainActivity.this));
                mPtrLayout.onRefreshComplete();
                mLvGames.setHasLoadMore(true);
                EmptyViewUtils.showLoading(mFlEmptyView);
            }
        });
        mPtrLayout.setLastUpdateTimeRelateObject(this);
        mLvGames.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void loadMore() {
                mLvGames.setAdapter(new ListAdapter(MainActivity.this));
                mLvGames.setHasLoadMore(true);
                mPtrLayout.onRefreshComplete();
                EmptyViewUtils.showLoading(mFlEmptyView);
            }
        });
        mPtrLayout.autoRefresh();
    }

    public void setStatusBar(boolean flag) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (flag) {
                getWindow().addFlags(
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                tintManager = new SystemBarTintManager(this);
                tintManager.setStatusBarTintEnabled(flag);
                tintManager.setStatusBarTintResource(Color.TRANSPARENT);
            } else {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
        }
    }
}
